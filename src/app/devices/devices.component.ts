import { Component } from '@angular/core';
import { DeviceService } from "../services/devices.service";


@Component({
  selector: '[devices]',
  templateUrl: './devices.template.html',
  styleUrls: [ './devices.style.scss' ]
})

export class Devices {

  devices;
  tokenDevice;  
  constructor(private deviceService: DeviceService) {    
    this.refreshDevices();
        
   }

   refreshDevices(){
     this.deviceService.getDevices().subscribe(data =>{
      console.log("data"+data);
      this.devices=data;
    });
   }

   changeActive(e, idDevice){     
        this.deviceService.changeDeviceStatus(idDevice).subscribe();

   }

   deleteDevice(idDevice){     
           this.deviceService.deleteDevice(idDevice).subscribe(data =>{
          //if(data.errorCodeList.length==0){            
            this.refreshDevices();
          //}else{
            //alert(data.errorMessageList[0]);
          //}
        },
      )
   }

   validateNewDevice(){
    
      this.deviceService.validateNewDevice(this.tokenDevice).subscribe(data =>{
          if(data.errorCodeList.length==0){            
            this.refreshDevices();
          }else{
            alert(data.errorMessageList[0]);
          }
      },
    )
   }

  printInvoice() {
    window.print();
  }
  

  

}

