import { Component } from '@angular/core';
import { DeviceService } from "../../services/devices.service";
import { ActivatedRoute } from '@angular/router';
import { DeviceDto } from "../../models/deviceDto";
import { DeviceTypeDto } from "../../models/deviceTypeDto";
import { BasicResponseDto } from "../../models/basicResponseDto";

@Component({
  selector: '[deviceDetail]',
  templateUrl: './deviceDetail.template.html',
  styleUrls: [ './deviceDetail.style.scss' ]
})

export class DeviceDetail {

  device;
  basicResponse;
  idDevice;

  conectivityRequired;


  constructor(private deviceService: DeviceService,private route: ActivatedRoute) {
     this.idDevice=this.route.snapshot.params['id'];
     this.loadForm();
   }
    

   loadForm(){
     this.generateDefaultDevice()
     if(this.idDevice !=0){        
        this.deviceService.getDeviceDetail(this.idDevice).subscribe((data: DeviceDto)=> { 
              this.device=data            
              console.log(data);
            }); 
            
     }else{
        
     }      
      console.log("thisDevice"+this.device.active);
   }
   
   saveDeviceDetail(){     
      this.deviceService.saveDeviceDetail(this.device).subscribe((data: BasicResponseDto)=> { 
              this.basicResponse=data            
              console.log(this.basicResponse);
            }); 

     

   }

   validate(){
     if(this.device.conectivity == null || this.device.conectivity == ""){
        
     }
   }

   deleteDeviceDetailForm(){
      
      // this.device.mode = null;
       this.device.manufacturer = null;
       this.device.model = null;
       this.device.imageUrl = null;

       //Device properties
       this.device.conectivity = null;
       this.device.mac = null;
       this.device.ip = null;
      // this.device.uid  = null;
       this.device.serialNumber  = null;

       //Device status
       this.device.active = false;

       //Device environment
      // this.device.activeEnv = false;
      // this.device.entity = null;
      // this.device.photoList = null;
      // this.device.location = null;
       //this.device.identificatorEnv = null;

     //  this.device.imageBigUrl = null;
   }

   generateDefaultDevice(){

     var deviceTypeDto : DeviceTypeDto = {
        id: "",
        name: "",
        imageUrl: ""
     }


     var deviceDto : DeviceDto = {
        id: "",
        name: "",
        active: true,
        conectivity: "",
        ssid: "",
        mac: "",
        ip: "",
        iBeaconMajor: "",
        iBeaconMinor: "",
        udid: "",
        serialNumber: "",
        brand: "",
        manufacturer: "",
        model: "",
        platform: "",
        version: "",
        idPush: "",
        online: true,
        paperStatus: "",
        battery: "",
        identificator: "",
        notes: "",
        lastConnectionDate: new Date(),
        imageUrl: "",
        deviceType: deviceTypeDto,
        deviceCarousel: null,
        deviceLogList: null,
        errorCodeList: null,
        errorMessageList: null
     }
 
      this.device=deviceDto;
   }

  printInvoice() {
    window.print();
  }
}
