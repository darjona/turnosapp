import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DeviceDetail } from './deviceDetail.component';

export const routes = [
  { path: '', component: DeviceDetail, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    DeviceDetail
    
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class DeviceDetailModule {
  static routes = routes;
}
