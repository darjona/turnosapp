import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Devices } from './devices.component';

export const routes = [
  { path: '', component: Devices, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    Devices
    
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class DevicesModule {
  static routes = routes;
  
}
