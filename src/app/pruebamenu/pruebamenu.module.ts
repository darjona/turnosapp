import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ElementoMenu } from './elementomenu/elementomenu.component';


export const routes = [
  {path: '', redirectTo: 'elementomenu', pathMatch: 'full'},
  {path: 'elementomenu', component: ElementoMenu}
 // {path: 'inbox', loadChildren: './inbox/inbox.module#InboxModule' }
];

@NgModule({
  declarations: [
    // Components / Directives/ Pipes
    ElementoMenu
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class PruebamenuModule {
  static routes = routes;
}
