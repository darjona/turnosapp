import { Routes, RouterModule }  from '@angular/router';
import { Layout } from './layout.component';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  { path: '', component: Layout, children: [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    { path: 'dashboard', loadChildren: '../dashboard/dashboard.module#DashboardModule' },
    { path: 'devices', loadChildren: '../devices/devices.module#DevicesModule' },
    { path: 'devices/deviceDetail/:id', loadChildren: '../devices/deviceDetail/deviceDetail.module#DeviceDetailModule' },
    { path: 'desks', loadChildren: '../desks/desks.module#DesksModule' },
    { path: 'desks/deskDetail', loadChildren: '../desks/deskDetail/deskDetail.module#DeskDetailModule' },
    { path: 'form', loadChildren: '../form/form.module#FormModule' },
    { path: 'statistics', loadChildren: '../statistics/statistics.module#StatisticsModule' },
    { path: 'ui', loadChildren: '../ui/ui.module#UiModule' },
    { path: 'components', loadChildren: '../components/components.module#ComponentsModule' },
    { path: 'tables', loadChildren: '../tables/tables.module#TablesModule' },
    { path: 'widgets', loadChildren: '../widgets/widgets.module#WidgetsModule' },
    { path: 'special', loadChildren: '../special/special.module#SpecialModule' },
    { path: 'pruebamenu', loadChildren: '../pruebamenu/pruebamenu.module#PruebamenuModule' }//newmenu
  ]}
];

export const ROUTES = RouterModule.forChild(routes);
