import { DeviceTypeDto } from "../models/deviceTypeDto";
import { DeviceCarouselImageDto } from "../models/deviceCarouselImageDto";
import { DeviceLogDto } from "../models/deviceLogDto";



export class DeviceDto{
    
   constructor(
       //Head
       public id: string,
       public name: string,
       public active: boolean,
       public conectivity: string,
       public ssid: string,
       public mac: string,
       public ip: string,
       public iBeaconMajor: string,
       public iBeaconMinor: string,
       public udid: string,
       public serialNumber: string,
       public brand: string,
       public manufacturer: string,
       public model: string,
       public platform: string,
       public version: string,
       public idPush: string,
       public online: boolean,
       public paperStatus: string,
       public battery: string,
       public identificator: string,
       public notes: string,
       public lastConnectionDate: Date,
       public imageUrl: string,
       public deviceType: DeviceTypeDto,
       public deviceCarousel: DeviceCarouselImageDto[],
       public deviceLogList: DeviceLogDto[],
       public errorCodeList : String[],
	   public errorMessageList : String[]
   ) {}
}


