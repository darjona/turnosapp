export class DeskDto {
   constructor(
      public id: string,
      public name: string,
      public imageUrl: string,
      public description: string,
      public inProgress: number,
      public inQueue: number,
      public averageTime: number,
      public appCheck: boolean,
      public smsCheck: boolean,
      public ticketCheck: boolean,
      public active: boolean
   ) {}
}


