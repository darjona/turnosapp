export class DeviceDetailDto {
   constructor(
       //Head
       public id: string,
       public mode: string,
       public manufacturer: string,
       public model: string,
       public imageUrl: string,

       //Device properties
       public conectivity: string,
       public mac: string,
       public ip: string,
       public uid: string,
       public serialNumber: string,

       //Device status
       public active:boolean,

       //Device environment
       public activeEnv:boolean,
       public entity:string,
       public photoList:Array<String>,
       public location: string,
       public identificatorEnv: string,

       public imageBigUrl: string
   ) {}
}


