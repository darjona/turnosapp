export class DeviceCarouselImageDto {
   constructor(
       //Head
       public id: string,
       public name: string,
       public imageUrl: string
   ) {}
}


