export class UserDto {
   constructor(
      public id: string,
      public name: string,
      public username: string,
      public email: string
   ) {}
}


