export class DeviceLogDto {
   constructor(
       //Head
       public id: string,
       public idDevice: string,
       public line: string,
       public logDate: Date
   ) {}
}


