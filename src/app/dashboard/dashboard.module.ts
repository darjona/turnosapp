import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import 'jquery-slimscroll';

import { Dashboard } from './dashboard.component';
import { Chat } from './chat/chat.component';
import { WidgetModule } from '../layout/widget/widget.module';

import { Nvd3ChartModule } from './nvd3/nvd3.module';
import { JqSparklineModule } from '../layout/directives/sparkline/sparkline.module';


//nuevo

import 'jquery-ui';
import 'jquery-ui/ui/widgets/sortable.js';
import 'jquery-ui/ui/widgets/resizable.js';
import 'easy-pie-chart/dist/jquery.easypiechart.js';

import { MorrisChartModule } from '../layout/directives/morris/morris.module';

//hasta aqui nuevo


export const routes = [
  { path: '', component: Dashboard, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    Dashboard,
    Chat
  ],
  imports: [
    CommonModule,
    FormsModule,
    Nvd3ChartModule,
    WidgetModule,
    JqSparklineModule,
    RouterModule.forChild(routes),
    MorrisChartModule,
  ]
})
export class DashboardModule {
  static routes = routes;
}
