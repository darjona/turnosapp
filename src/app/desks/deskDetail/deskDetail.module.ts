import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { DeskDetail } from './deskDetail.component';

export const routes = [
  { path: '', component: DeskDetail, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    DeskDetail
    
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class DeskDetailModule {
  static routes = routes;
}
