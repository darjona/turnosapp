import { Component } from '@angular/core';
import { DeskService } from "../services/desks.service";

@Component({
  selector: '[desks]',
  templateUrl: './desks.template.html',
  styleUrls: [ './desks.style.scss' ]
})

export class Desks {

   desks;
  constructor(private deskService: DeskService) {
    this.desks=deskService.getDesks();
     // this.users = usergService.getUsers();
   }

  printInvoice() {
    window.print();    
  }
  

}
