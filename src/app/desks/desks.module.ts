import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import 'jquery-slimscroll';

import { Desks } from './desks.component';

export const routes = [
  { path: '', component: Desks, pathMatch: 'full' }
];

@NgModule({
  declarations: [
    Desks

  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
  ]
})
export class DesksModule {
  static routes = routes;
}
