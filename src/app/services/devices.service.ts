import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { DeviceDto } from "../models/deviceDto";
import { BasicResponseDto } from "../models/basicResponseDto";
import { HttpParams } from "@angular/common/http";

//import { DatePipe } from '@angular/common';

@Injectable()
export class DeviceService {
   constructor(private http: HttpClient) {
   }
 
   public getDevices(): Observable<DeviceDto[]> {
  
    var date = new Date();
 
    const params = new HttpParams()
    .set('id', '1')
    .set('idEncoded', '1');

   //this.http.get('http://localhost:8080/bb8/message/', {params}).subscribe(data => {
      // Read the result field from the JSON response.
     // console.log(data['results']);
    //});

    //this.http.get('http://localhost:8080/bb8/message/', {params}).subscribe(data => {
      //console.log(data);
    //});

    //this.http.get<DeviceDto[]>('http://localhost:8080/bb8/lista/', {params}).subscribe(data => {
      // console.log(Observable.of(data));
       //console.log(JSON.stringify(data));

    //});

    // EJEMPLO BUENO
   // return this.http.get<DeviceDto[]>('http://localhost:8080/bb8/lista/', {params});
    //console.log("holi"+response);

   // return Observable.of(deviceList);
   //return this.http.get("http://localhost:8080/bb8/lista/david")
     //   .map((res: Response) => res.json())
       //  .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
      //console.log("he llegado");
      //return Observable.of(data);
        // return null;

        

        return this.http.get<DeviceDto[]>('http://localhost:8080/bb8/getdevicelist/');
   }


   getDeviceDetail(id : string): Observable<DeviceDto> {
  
    
    var date = new Date();
    let idEncoded=btoa(id);

    const params = new HttpParams().set('idEncoded', idEncoded);
        
    //this.http.get<DeviceDto[]>('http://localhost:8080/bb8/getdevicelist/');
    
    
      return this.http.get<DeviceDto>('http://localhost:8080/bb8/getdevice/',{params});         
   }

    deleteDevice(id : string): Observable<BasicResponseDto> {
  
 
      let idEncoded=btoa(id);

      const params = new HttpParams().set('idEncoded', idEncoded);

            
      return this.http.get<BasicResponseDto>('http://localhost:8080/bb8/deletedevice/',{params});         
   }

   saveDeviceDetail(deviceDto : DeviceDto): Observable<BasicResponseDto> {
        let result = true;
        JSON.stringify(deviceDto);
        
        const params: HttpParams = new HttpParams().set("param",JSON.stringify(deviceDto));
        
        
        return this.http.post<BasicResponseDto>('http://localhost:8080/bb8/updatedevicedetail/', params);

   }


   validateNewDevice(token : string): Observable<DeviceDto> {        
        
        const params: HttpParams = new HttpParams().set("param",token);
                                
       return this.http.post<DeviceDto>('http://localhost:8080/bb8/validatenewdevice/', params);      
   }


   changeDeviceStatus(idDevice: string){

        let idEncoded=btoa(idDevice);

        const params = new HttpParams().set('idEncoded', idEncoded);

        return this.http.post('http://localhost:8080/bb8/changedevicestatus/', params);      
   }

   private extractObject(res: Response): Object {
    const data: any = res.json();
    return data || {};
  }
}
