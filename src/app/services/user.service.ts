import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { UserDto } from "../models/userDto";

@Injectable()
export class UserService {
   constructor(private http: Http) {
   }
 
   getUsers(): Observable<UserDto[]> {
      return this.http.get("https://jsonplaceholder.typicode.com/users")
         .map((res: Response) => res.json())
         .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
   }
}