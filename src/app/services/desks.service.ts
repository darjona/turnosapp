import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { DeskDto } from "../models/deskDto";
//import { DatePipe } from '@angular/common';

@Injectable()
export class DeskService {
   constructor(private http: Http) {
   }
 
   getDesks(): Observable<DeskDto[]> {
  
    var date = new Date();

    var desk1 = new DeskDto('id','nombre','assets/img/impresora.png','descripcion',10,10,50, true, true, true, true);
    var desk2 = new DeskDto('id2','nombre2','assets/img/totem.png','descripcion',5,5,25, false, false, false, false);
    var deskList = [desk1, desk2];
    
  //  this.http.get("http://localhost:8080/bb8/lista/david")
    //     .map((res: Response) => res.json())
      //   .catch((error: any) => Observable.throw(error.json().error || 'Server error'))


    return Observable.of(deskList);
//      return this.http.get("https://jsonplaceholder.typicode.com/users")
//        .map((res: Response) => res.json())
//         .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
   }

}
